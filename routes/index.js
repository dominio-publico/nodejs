var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.json({nombre:"hola"});
});

router.get('/verCoches', function(req, res, next) {
  res.json({titulo:"Pagina de coches", cantidad:3});
});

module.exports = router;