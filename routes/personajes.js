var express = require('express');
var router = express.Router();
const PersonajesController = require('../controllers/PersonajesController')

router.get("/verPersonajes", PersonajesController.verPersonajes);
router.get("/favorito", PersonajesController.favorito);
router.get("/borrarPersonaje/:id", PersonajesController.borrarPersonaje);

module.exports = router;
