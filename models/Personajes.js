const mongoose = require("mongoose")
const Schema = mongoose.Schema

const PersonajeSchema = new Schema({
    id: {type:Number, required: true},
    name: String,
    image: String,
    species: String
})

module.exports = mongoose.model('characters', PersonajeSchema)